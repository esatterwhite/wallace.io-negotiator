/*jshint laxcomma:true, smarttabs: true */

/**
 * DESCRIPTION
 * @module NAME
 * @author
 * @requires moduleA
 * @requires moduleB
 * @requires moduleC
 **/
var jstoxml    = require('jstoxml')
	, path     = require("path")
	, formats
	, content_types
	, serialize
	, object
	;

formats			= ['json', 'xml'];
content_types	= {
    'json': ['application/json', "text/json" ] ,
    'xml': [ 'application/xml', "text/xml"]
};

/**
 * DESCRIPTION
 * @method NAME
 * @param {TYPE} NAME ...
 * @param {TYPE} NAME ...
 * @return
 **/
exports.determineFormat = function( accepts ){

	var format = null
	  , key;
	for ( key in content_types ){
		var item = content_types[ key ];
		for( var type = 0; type < item.length; type++){
			if( item[ type ] == accepts ){
				format = key
				break;
			}
		}
	}

	return format;
};

serialize = function(request, response, data ){

	var accepts = request.headers["accept"]
		,desired_format;

	desired_format = exports.determineFormat( accepts )

	if( !desired_format ){
		response.status( 500 )
		response.charset = "utf-8"
		response.set("Content-Type", "text/plain")
		return resposne.send( "Unsported Format" )
	}

	return exports.encode(data, desired_format, "root" );
};

/**
 * DESCRIPTION
 * @method NAME
 * @param {TYPE} NAME ...
 * @param {TYPE} NAME ...
 * @return
 **/
exports.mime = function( key ){
	if( content_types.hasOwnProperty( key ) ){
		return content_types[ key ][ 0 ];
	} else{
		return "text/plain"
	}
}

/**
 * DESCRIPTION
 * @method NAME
 * @param {TYPE} NAME ...
 * @param {TYPE} NAME ...
 * @return
 **/
exports.to_json = function( data ){
	return JSON.stringify( data )
};

/**
 * DESCRIPTION
 * @method NAME
 * @param {TYPE} NAME ...
 * @param {TYPE} NAME ...
 * @return
 **/
exports.to_xml = function( data ){
	return jstoxml.toXML(data, {header:true}, " ")
};

/**
 * DESCRIPTION
 * @method NAME
 * @param {TYPE} NAME ...
 * @param {TYPE} NAME ...
 * @return
 **/
exports.encode = function( data, format, wrap_xml ){
	var _fmt
	format = format || "json"

	_fmt = exports.determineFormat( format )

	if( !_fmt ){
		// try again
		if( formats.indexOf( format ) != -1 ){
			_fmt = format
		} else{
			// default to json
			_fmt = "json"
		}
	}
	if( _fmt =="xml" && wrap_xml ){
		var wrapper = {};

		wrapper[ wrap_xml ] = data;
		data = wrapper;
	}
	return exports["to_" + _fmt ]( data );
};

/**
 * DESCRIPTION
 * @method NAME
 * @param {TYPE} NAME ...
 * @param {TYPE} NAME ...
 * @return
 **/
exports.createResponse = function( request, response, data ){
	return serialize( request, response, data)
};

